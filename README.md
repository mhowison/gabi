GABI is developed by Mark Howison at the [Center for Computation and
Visualization](http://ccv.brown.edu), Brown University, in collaboration with
Casey W. Dunn and Felipe Zapata of the [Dunn Lab](http://dunnlab.org) and
[Erika J. Edwards](http://www.brown.edu/Research/Edwards_Lab).

# Overview of GABI

Genome Assembly by Bayesian Inference (GABI) is a prototype of a Bayesian
framework for sequence assembly.

An example report showing an assembly of the PhiX174 phage is available at:

[http://ccv.brown.edu/mhowison/gabi-report](http://ccv.brown.edu/mhowison/gabi-report)

# Installation

GABI is experimental software and is not ready for production use. To test it,
you will need to first install:

* [BioLite](https://bitbucket.org/caseywdunn/biolite) 0.4.0
* [LAP](http://assembly-eval.sourceforge.net) 1.1
* [Bowtie2](http://bowtie-bio.sourceforge.net/bowtie2) 2.1.0

These instructions assume you are installing to a directory `$PREFIX`, such as:

    export PREFIX=/usr/local

Alternatively, you might use your home directory as the prefix:

    export PREFIX=$HOME

To install GABI, run:

    ./configure --prefix=$PREFIX
    make install

To run GABI, make sure that `$PREFIX/bin` is in your path, and try:

    gabi help

See the [phix-test](https://bitbucket.org/mhowison/gabi/src/master/phix-test)
directory for instructions and sample data for testing GABI on the PhiX174
genome.

# Issues

If you find a bug or encounter a problem running GABI, please submit it through
the Bitbucket [issue tracker](https://bitbucket.org/mhowison/gabi/issues).

# Citing

We have a preprint describing GABI available on arXiv:

Howison M, Zapata F, Edwards EJ, Dunn CW. (2013) Bayesian genome assembly and
assessment by Markov Chain Monte Carlo sampling.
[arXiv:1308.1388](http://arxiv.org/abs/1308.1388)

# License

Copyright 2013-2014, Brown University, Providence, RI. All Rights Reserved.

GABI is distributed under the GNU General Public License version 3. For more
information, see LICENSE or visit:
[http://www.gnu.org/licenses/gpl.html](http://www.gnu.org/licenses/gpl.html)

