#!/bin/bash

set -e

export BIOLITE_RESOURCES="database=$PWD/biolite.sqlite"

DATE=`date +%s`
IDS=${1-`bl-diagnostics list -n sample | awk '/^\*/{print $2}' | tail -3`}
echo "automatically choosing run IDs:"
echo "$IDS"
echo "these should be correct if you have run the phix-test scripts in order"

gabi report -o report-$DATE $IDS

