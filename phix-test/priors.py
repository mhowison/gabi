#!/usr/bin/env python
# vim: set fileencoding=utf-8
#
# GABI - Genome Assembly by Bayesian Inference
#
# Copyright 2013-2014, Brown University, Providence, RI. All Rights Reserved.
#
# This file is part of GABI.
#
# GABI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# GABI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GABI.  If not, see <http://www.gnu.org/licenses/>.

import math
import matplotlib
matplotlib.use('Agg')

import numpy as np
import sys

from matplotlib.pyplot import *

from gabi import consensus
from gabi import trace
from gabi.graph import Graph

matplotlib.rcParams.update({'font.size': 8})

letters = 'abcde'

def split_frequency_plot(names, graph, traces, outname):
    """
    Split frequencies at the final sample for each trace, compared against
    the first trace.
    """

    frequencies = []

    for path in traces:
        trace.load(path)
        matrices = trace.get_incidence_matrices(graph.E, graph.N)
        frequencies.append(consensus.calc_frequencies(*matrices))

    edges = set()
    map(edges.update, frequencies)
    edges = sorted(edges, key=(lambda e : frequencies[0].get(e, 0.0)))

    figure(figsize=(6,8))

    x = np.array([frequencies[0].get(e, 0.0) for e in edges])
    for i in xrange(0, len(names)):
        subplot(len(names), 1, i+1)
        title('(%c) %s' % (letters[i], names[i]), size=9, weight='heavy')
        xlim([0, len(edges)])
        xticks([])
        grid(axis='x')
        if i == len(names)-1:
            xlabel("Edges/nodes sorted by ascending frequency at k=10")
        ylim([-0.2, 0.2])
        yrange = [n/10.0 for n in xrange(-2, 3)]
        yticks(yrange, ["%.0f%%" % (100.0*y) for y in yrange])
        if i == len(names)/2:
            ylabel(u"Change in frequency vs. k=10 chain")
        y = np.array([frequencies[i+1].get(e, 0.0) for e in edges])
        y -= x
        axhline(y=0, alpha=0.5, c='k', lw=0.5)
        for i in xrange(0, len(edges)):
            c = min(1.0, math.sqrt(abs(4*y[i])))
            bar(i + 0.1, y[i], linewidth=0, facecolor=(c,0,0))
        #bar(np.arange(len(edges)) + 0.1, y, color=get_cmap('YlOrRd'))

    tight_layout()
    savefig(outname)


if __name__ == '__main__':
    names = sys.argv[1].split(',')
    print names
    graph = Graph(sys.argv[2])
    split_frequency_plot(names, graph, sys.argv[3:], 'priors.pdf')

