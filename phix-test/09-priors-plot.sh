#!/bin/bash

set -e

export BIOLITE_RESOURCES="database=$PWD/biolite.sqlite"

OUTDIR=$PWD/analyses/phix

IDS=`bl-diagnostics list -n sample | awk '/^\*/{print $2}' | head -1`
IDS="$IDS `bl-diagnostics list -n sample | awk '/^\*/{print $2}' | tail -5`"
echo "automatically choosing run IDs:"
echo $IDS
echo "these should be correct if you have run the phix-test scripts in order"

TRACES=""
for id in $IDS
do
	TRACES="$TRACES $OUTDIR/$id/mcmc.trace.sqlite"
done

$(dirname $0)/priors.py "no data,flat prior,k=5,k=3,k=1" cluster0.graphml $TRACES

