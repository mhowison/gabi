#!/bin/bash

wget ftp://webdata:webdata@ussd-ftp.illumina.com/Data/SequencingRuns/PhiX/PhiX_S1_L001_R1_001.fastq.gz
wget ftp://webdata:webdata@ussd-ftp.illumina.com/Data/SequencingRuns/PhiX/PhiX_S1_L001_R2_001.fastq.gz
gunzip PhiX_S1_L001_R1_001.fastq.gz
gunzip PhiX_S1_L001_R2_001.fastq.gz

bl-filter-illumina -i PhiX_S1_L001_R1_001.fastq -i PhiX_S1_L001_R2_001.fastq -o subset.1.fq -o subset.2.fq -n 2000 -s '/' -q 37 -a

curl "http://www.ncbi.nlm.nih.gov/sviewer/viewer.fcgi?tool=portal&sendto=on&log$=seqview&db=nuccore&dopt=fasta&val=9626372&extrafeat=0&maxplex=1" >phix-reference-NC_001422.fa

