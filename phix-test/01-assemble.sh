#!/bin/bash
#SBATCH -t 15
#SBATCH -c 8

set -e

gunzip -c subset.1.fq.gz > subset.1.fq
gunzip -c subset.2.fq.gz > subset.2.fq

gabi debruijn --circular 99 subset.?.fq

VelvetOptimiser.pl -s 99 -e 99 -f "-fastq -shortPaired -separate subset.*.fq"

mkdir -p sga
cd sga
sga preprocess --pe-mode 1 -o reads.pp.fastq ../subset.?.fq
sga index -a ropebwt --no-reverse reads.pp.fastq
sga correct -k 41 --learn -o reads.ec.fastq reads.pp.fastq
sga index -a ropebwt reads.ec.fastq
sga filter -x 2 reads.ec.fastq
sga overlap -m 85 reads.ec.filter.pass.fa
sga assemble -m 111 --min-branch-length 400 -o initial reads.ec.filter.pass.asqg.gz
cd ..

cat phix-reference-NC_001422.fa >phix-reference-NC_001422-double.fa
awk 'NR>1' phix-reference-NC_001422.fa >>phix-reference-NC_001422-double.fa

gabi compare --graph cluster0.graphml sga/initial-contigs.fa auto_data_99/contigs.fa phix-reference-NC_001422-double.fa

