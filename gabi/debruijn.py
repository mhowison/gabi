#!/usr/bin/env python
#
# GABI - Genome Assembly by Bayesian Inference
#
# Copyright 2013-2014, Brown University, Providence, RI. All Rights Reserved.
#
# This file is part of GABI.
#
# GABI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# GABI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GABI.  If not, see <http://www.gnu.org/licenses/>.

"""
Build a de bruijn graph and split it into connected components, retaining
only those that are larger than a given threshold. Pickle the graph to a
file.
"""

import argparse
import os
import psutil
import re
import time

from Bio.Seq import reverse_complement
from Bio.SeqIO.QualityIO import FastqGeneralIterator

parser = argparse.ArgumentParser(
            formatter_class=argparse.RawDescriptionHelpFormatter,
            epilog=__doc__)
parser.add_argument('K', type=int, help="k-mer size")
parser.add_argument('READS', nargs='+', help="FASTQ files with input reads")
parser.add_argument('--circular', action="store_true", default=False, help="""
    genome is circular: trim all tips""")
parser.add_argument('--tips', type=int, metavar='N', default=0, help="""
    trim up to N consecutive tips (ignored if --circular)""")
parser.add_argument('-n', type=int, default=1, help="""
    only store clusters with more than n sites""")
args = parser.parse_args()

start_time = time.time()
proc = psutil.Process(os.getpid())

_graphml_header = """<?xml version="1.0" encoding="utf-8"?><graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
<key attr.name="seq" attr.type="string" for="node" id="seq"/>
<key attr.name="kmer" attr.type="string" for="node" id="kmer"/>
<graph edgedefault="directed">"""
_graphml_footer = """</graph>
</graphml>"""

def log(msg):
    print "[%10.2f] %s" % (time.time() - start_time, msg)

def done():
    mem = proc.get_memory_info()
    log("done. / mem: {:7,d}MB / vmem: {:7,d}MB /".format(*[m/1048576 for m in mem]))

class Node:
    i = 1
    def __init__(self, seq):
        self.seq = seq
        self.kmer = None
        self.edges_in = set()
        self.edges_out = set()
        self.visited = False
        self.id = Node.i
        Node.i += 1
    def edge_to(self, n):
        if self != n and not n in self.edges_out:
            self.edges_out.add(n)
            n.edges_in.add(self)
            return 1
        else:
            return 0
    def delete(self):
        for n in self.edges_in:
            n.edges_out.remove(self)
        for n in self.edges_out:
            n.edges_in.remove(self)

K = args.K
print "kmer length", K

alphabet = re.compile(r"^[ATCG]+$")

log("Hashing reads...")

nreads = 0
kmers = {}

for fastq in args.READS:
    for _, seq, _ in FastqGeneralIterator(open(fastq)):
        nreads += 1
        for i in range(0, len(seq)-K+1):
            kmer = seq[i:i+K]
            if alphabet.match(kmer):
                kmers[kmer] = kmers.get(kmer, Node(kmer[-1]))
                rkmer = reverse_complement(kmer)
                kmers[rkmer] = kmers.get(rkmer, Node(rkmer[-1]))

print "{:,d} reads / {:,d} kmers".format(nreads, len(kmers))

done()

log("Building edge list...")

nedges = 0

for kmer in kmers:
    for b in ('A','C','G','T'):
        neighbor = kmer[1:] + b
        if neighbor in kmers:
            nedges += kmers[kmer].edge_to(kmers[neighbor])

print "{:,d} nodes / {:,d} edges".format(len(kmers), nedges)

done()

log("Removing singleton nodes...")

nodes = set()
for kmer, node in kmers.iteritems():
    if node.edges_out or node.edges_in:
        node.kmer = kmer
        nodes.add(node)

print "removed {:,d} nodes".format(len(kmers) - len(nodes))
print "{:,d} nodes / {:,d} edges".format(len(nodes), nedges)

done()

log("Removing tips...")

ntails = 0
nheads = 0

if args.circular:
    # remove all tips
    del kmers
    tails = [n for n in nodes if not n.edges_in]
    while tails:
        node = tails.pop()
        if node in nodes and not node.edges_in:
            tails += node.edges_out
            nedges -= len(node.edges_out)
            node.delete()
            nodes.remove(node)
            ntails += 1
    heads = [n for n in nodes if not n.edges_out]
    while heads:
        node = heads.pop()
        if node in nodes and not node.edges_out:
            heads += node.edges_in
            nedges -= len(node.edges_in)
            node.delete()
            nodes.remove(node)
            nheads += 1
else:
    if args.tips:
        tails = [n for n in nodes if not n.edges_in]
        for n in tails:
            for _ in xrange(args.tips):
                if not (n in nodes and len(n.edges_out) == 1):
                    break
                nedges -= 1
                ntails += 1
                n1 = next(iter(n.edges_out))
                n.delete()
                nodes.remove(n)
                n = n1
        heads = [n for n in nodes if not n.edges_out]
        for n in heads:
            for _ in xrange(args.tips):
                if not (n in nodes and len(n.edges_out) == 1):
                    break
                nedges -= 1
                nheads += 1
                n1 = next(iter(n.edges_in))
                n.delete()
                nodes.remove(n)
                n = n1
    # place kmers at front of the tips
    for kmer, node in kmers.iteritems():
        if node and not node.edges_in:
            node.seq = kmer
    del kmers

print "removed {:,d} forward tips".format(ntails)
print "removed {:,d} reverse tips".format(nheads)
print "{:,d} nodes / {:,d} edges".format(len(nodes), nedges)

done()

log("Merging redundant nodes...")

nred = 0

for node in list(nodes):
    if node in nodes:
        while len(node.edges_out) == 1:
            neighbor = next(iter(node.edges_out))
            assert neighbor in nodes
            if len(neighbor.edges_in) == 1:
                assert next(iter(neighbor.edges_in)) == node
                node.seq += neighbor.seq
                neighbor.delete()
                nodes.remove(neighbor)
                map(node.edge_to, neighbor.edges_out)
                nedges -= 1
                nred += 1
            else:
                break

print "merged {:,d} redundant nodes".format(nred)
print "{:,d} nodes / {:,d} edges".format(len(nodes), nedges)

done()

log("Finding clusters...")

nclusters = 0
nvisited = 0

for node in nodes:
    if not node.visited:
        queue = [node]
        cluster = set()
        # Follow all edges.
        while queue:
            node = queue.pop()
            if not node.visited:
                nvisited += 1
                if not nvisited % 10000:
                    log("%d nodes visited..." % nvisited)
                cluster.add(node)
                queue += node.edges_out
                queue += node.edges_in
                node.visited = True
        nsites = sum(len(node.seq) for node in cluster)
        if nsites > args.n:
            with open('cluster%d.graphml' % nclusters, 'w') as f:
                print >>f, _graphml_header
                nnodes = 0
                nref = 0
                for node in cluster:
                    node.id = nnodes
                    print >>f, '<node id="%d"><data key="seq">%s</data><data key="kmer">%s</data></node>' % (node.id, node.seq, node.kmer)
                    nnodes += 1
                nedges = 0
                for node in cluster:
                    for neighbor in node.edges_out:
                        print >>f, '<edge source="%d" target="%d"/>' % (node.id, neighbor.id)
                        nedges += 1
                print >>f, _graphml_footer
            print "cluster {:,d}: {:,d} nodes / {:,d} edges / {:,d} sites".format(
                                    nclusters, len(cluster), nedges, nsites)
            nclusters += 1

done()
