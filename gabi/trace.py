#
# GABI - Genome Assembly by Bayesian Inference
#
# Copyright 2013-2014, Brown University, Providence, RI. All Rights Reserved.
#
# This file is part of GABI.
#
# GABI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# GABI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GABI.  If not, see <http://www.gnu.org/licenses/>.

import ast
import numpy as np
import os
import sqlite3
from biolite import utils
from gabi.sampler import Sample

_db = None
_inserts = 0

samples_schema = (
    ('id', 'INTEGER PRIMARY KEY NOT NULL'),
    ('odds', 'REAL'),
    ('likelihood', 'REAL'),
    ('prior', 'REAL'),
    ('length', 'INTEGER'),
    ('ncontigs', 'INTEGER'),
    ('accept', 'INTEGER'),
    ('runtime', 'REAL'),
    ('edges', 'TEXT'),
    ('nodes', 'TEXT'))


def _create_sql(name, schema, index):
    sql = [" CREATE TABLE %s (" % name]
    sql += ["   %s %s," % s for s in schema[:-1]]
    sql += ["   %s %s);" % schema[-1]]
    sql += [" CREATE INDEX {0}_{1} ON {0}({1});".format(name, i) for i in index]
    return '\n'.join(sql)


def connect(path):
    """
    Establish a gobal database connection and create the tables if the
    database did not exist.
    """
    global _db
    if _db:
        _db.close()
    exists = os.path.exists(path)
    _db = sqlite3.connect(path, isolation_level=None)
    if not exists:
        _db.executescript(_create_sql(
            'samples', samples_schema, ['odds', 'accept', 'runtime']))
    else:
        last = int(next(_db.execute("SELECT MAX(id) FROM samples;"))[0])
        utils.info(
            "restarting from previous trace '%s' at iteration %d" % (
            path, last))
        return last


def load(path):
    """
    Establish a global database connection to an existing trace.
    """
    global _db
    if _db:
        _db.close()
    _db = sqlite3.connect(path, isolation_level=None)


def last_accepted():
    """
    Returns the id of the last accepted sample.
    """
    sql = "SELECT MAX(id) FROM samples WHERE accept=1;"
    return int(next(_db.execute(sql))[0])


def get_sample(i):
    """
    Returns a Sample object for `id`.
    """
    sql = """
        SELECT likelihood, prior, length, ncontigs
        FROM samples
        WHERE id=%d;""" % i
    return Sample(*next(_db.execute(sql)))


def get_state(i):
    sql = "SELECT edges, nodes FROM samples WHERE id=%d;" % i
    return next(_db.execute(sql))


def get_edges():
    """
    Returns an iterator over lists of the edges in each accepted sample.
    """
    sql = "SELECT edges FROM samples WHERE accept=1 ORDER BY id;"
    for row in _db.execute(sql):
        yield row[0]


def get_nodes():
    """
    Returns an iterator over lists of the nodes in each accepted sample.
    """
    sql = "SELECT nodes FROM samples WHERE accept=1 ORDER BY id;"
    for row in _db.execute(sql):
        yield row[0]


def get_incidence_matrices(nedges, nnodes):
    """
    Returns two boolean numpy matrices with the edge and node incidence, where the
    columns are the edge/node id, and the rows are the accepted samples.
    """
    sql = "SELECT COUNT(*) FROM samples WHERE accept=1;"
    naccept = int(_db.execute(sql).fetchone()[0])
    edges = np.zeros((nedges, naccept), dtype=np.bool)
    nodes = np.zeros((nnodes, naccept), dtype=np.bool)
    sql = "SELECT edges, nodes FROM samples WHERE accept=1 ORDER BY id;"
    for i, row in enumerate(_db.execute(sql)):
        for e in ast.literal_eval(row[0]):
            edges[e][i] = True
        for n in ast.literal_eval(row[1]):
            nodes[n][i] = True
    return edges, nodes


def get_stats(stats={}):
    """
    Populates the following entries in `stats`:
    * `nsamples` - total number of samples
    * `accept` - number of accepted samples
    * `accept_per` - percent of accepted samples
    * `mintime` - minimum sampling runtime
    * `maxtime` - maximum sampling runtime
    * `time` - total sampling runtime
    """
    sql = """
        SELECT COUNT(runtime), MIN(runtime), MAX(runtime), SUM(runtime)
        FROM samples;"""
    vals = map(float, _db.execute(sql).fetchone())
    stats['nsamples'], stats['mintime'], stats['maxtime'], stats['time'] = vals
    sql = "SELECT COUNT(*) FROM samples WHERE accept=1;"
    stats['accept'] = int(_db.execute(sql).fetchone()[0])
    stats['accept_per'] = 100.0 * stats['accept'] / stats['nsamples']
    return stats


def get_likelihoods():
    """
    Returns an iterator of likelihoods for accepted samples.
    """
    sql = "SELECT likelihood FROM samples WHERE accept=1 ORDER BY id;"
    for row in _db.execute(sql):
        yield row[0]


def list2str(l):
    return str(l).replace(' ','')


def add_debug(i, sample, accept, runtime, edges, nodes):
    print \
        i, sample.odds, sample.likelihood, sample.prior, sample.length, \
        sample.ncontigs, accept, runtime, list2str(edges), list2str(nodes)


def add_db(i, sample, accept, runtime, edges, nodes):
    global _inserts
    # Insert with transaction.
#    if _inserts % 100 == 0:
    _db.execute("BEGIN")
    _db.execute(
        "INSERT INTO samples VALUES (?,?,?,?,?,?,?,?,?,?);", (
            i, sample.odds, sample.likelihood, sample.prior, sample.length,
            sample.ncontigs, accept, runtime, list2str(edges), list2str(nodes)
         )
     )
#    if _inserts % 100 == 99:
    _db.execute("COMMIT")
    _inserts += 1


add = add_db
