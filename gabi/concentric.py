#!/usr/bin/env python

import re
import subprocess
import sys

out = []
svgs = []

for dot in sys.argv[1:]:
	svgs.append(
		subprocess.check_output(['dot', '-Tsvg', '-Kneato', dot]).split('\n'))

out += svgs[0][:-2]

size = re.search(r'width="(\d+)pt" height="(\d+)pt"', svgs[0][6]).groups()
cx = float(size[0]) / 2.0
cy = float(size[1]) / 2.0
scale = 0.2

for i, svg in enumerate(svgs[1:], start=1):
	transform = "translate({0} {1}) scale({2} {2})".format(scale*cx, scale*cy, 1.0-scale)
	scale += 0.2 * (0.8 ** i)
	out.append(
		svg[8].replace('graph0', 'graph%d' % i).replace('scale(1 1)', transform).replace('transform', 'fill-opacity="0.0" transform'))
	out += svg[9:-2]

out.append('</svg>')

print '\n'.join(out)

