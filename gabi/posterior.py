#!/usr/bin/env python
#
# GABI - Genome Assembly by Bayesian Inference
#
# Copyright 2013-2014, Brown University, Providence, RI. All Rights Reserved.
#
# This file is part of GABI.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any purpose other than its incorporation into a
# commercial product is hereby granted without fee, provided that the
# above copyright notice appear in all copies and that both that
# copyright notice and this permission notice appear in supporting
# documentation, and that the name of Brown University not be used in
# advertising or publicity pertaining to distribution of the software
# without specific, written prior permission.
#
# BROWN UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
# INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR ANY
# PARTICULAR PURPOSE.  IN NO EVENT SHALL BROWN UNIVERSITY BE LIABLE FOR
# ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

import math
import networkx as nx
import os
import sys

from collections import namedtuple
from operators import itemgetter

from Bio import SeqIO

from biolite import utils


def load_fasta(filename):
	records = {}
	for record in SeqIO.parse(open(filename), 'fasta'):
		records[record.id] = str(record.seq)
	return records


def align_edges(query, reference, identity=99.9):
	"""
	"""
	subprocess.check_call(['makeblastdb', '-in', reference, '-dbtype', 'nucl'])
	swipe = subprocess.check_output(['swipe', '-p', 'blastn', '-i', query, '-d', reference, '-m', '8'])

	edges = load_fasta(query)
	alignments = {}

	# Parse SWIPE output, with fields:
	# 0 Query id
	# 1 Subject id
	# 2 % identity
	# 3 alignment length
	# 4 mismatches
	# 5 gap openings
	# 6 q. start
	# 7 q. end
	# 8 s. start
	# 9 s. end
	# 10 e-value
	# 11 bit score
	for line in swipe:
		fields = line.rstrip().split()
		assert len(fields) == 12
		e = fields[0]
		if fields[5] == '0' and float(fields[2]) > identity and int(fields[3]) == len(edges[e]):
			start = int(fields[8])
			alignments[subject].append(

	return []


if __name__ == '__main__':
	basename = sys.argv[1]
	graph = nx.read_graphml(basename + '.graphml')
	edges = align_edges(basename + '.fa', sys.argv[2])


