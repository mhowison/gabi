#!/usr/bin/env python
#
# GABI - Genome Assembly by Bayesian Inference
#
# Copyright 2013-2014, Brown University, Providence, RI. All Rights Reserved.
#
# This file is part of GABI.
#
# GABI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# GABI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GABI.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import subprocess
import sys
from Bio import SeqIO
from biolite import utils
from gabi import trace
from gabi.graph import Graph


def calc_frequencies(edge_matrix, node_matrix):
    n = float(edge_matrix.shape[1])
    assert float(node_matrix.shape[1]) == n
    edges = np.sum(edge_matrix, axis=1) / n
    nodes = np.sum(node_matrix, axis=1) / n
    result = {}
    for i, x in enumerate(edges):
        result['edge%d' % i] = x
    for i, x in enumerate(nodes):
        result['node%d' % i] = x
    return result


def posterior_plot(graph, frequencies, prefix):
    graph.write_dot(prefix+'-posteriors.dot', frequencies)
    subprocess.check_call(
        ['dot', '-Tsvg', '-Kneato', prefix+'-posteriors.dot'],
        stdout=open(prefix+'-posteriors.svg', 'w'))


def posterior_plot_add_stroke(graph, frequencies, prefix):
    with open(prefix+"-posteriors.svg", 'w') as f:
        # hack the SVG output to have an edge with both stroke and fill
        for line in svg.split('\n'):
            if line.startswith("<path"):
                # parse the attributes of the <path> tag
                attrs = dict(
                    x.split('="') for x in line[6:].rstrip('"/>').split('" '))
                # print two copies of <path>, one for the stroke and one for
                # the fill
                print >>f, '<path class="edge-stroke" stroke="grey" stroke-weight="7" d="%s"/>' % attrs['d']
                print >>f, '<path class="edge-fill" stroke="%s" stroke-weight="3" d="%s"/>' % (attrs['stroke'], attrs['d'])
            elif line.startswith("<ellipse"):
                print >>f, '<ellipse stroke-weight="3"', line[9:]
            else:
                print >>f, line


def majority_rule_plot(graph, frequencies, threshold, prefix):
    colors = {}
    keep = {}
    for e, f in frequencies.iteritems():
        if f > threshold:
            colors[e] = 'black'
            keep[e] = f
        else:
            colors[e] = 'grey70'
    graph.write_dot(prefix+'-majority.dot', colors=colors)
    subprocess.check_call(
        ['dot', '-Tsvg', '-Kneato', prefix+'-majority.dot'],
        stdout=open(prefix+'-majority.svg', 'w'))
    SeqIO.write(
        graph.contigs_annotated(keep),
        open(prefix+"-majority.fa", 'w'),
        "fasta")


if __name__ == '__main__':
    if utils.which('dot') is None:
        utils.die("could not find 'dot' command from graphviz")
    graph = Graph(sys.argv[1])
    trace.load(sys.argv[2])
    matrices = trace.get_incidence_matrices(graph.E, graph.N)
    frequencies = calc_frequencies(*matrices)
    posterior_plot(graph, frequencies, "consensus")
    majority_rule_plot(graph, frequencies, float(sys.argv[3]), "consensus")

