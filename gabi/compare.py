#!/usr/bin/env python
#
# GABI - Genome Assembly by Bayesian Inference
#
# Copyright 2013-2014, Brown University, Providence, RI. All Rights Reserved.
#
# This file is part of GABI.
#
# GABI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# GABI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GABI.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import os
import subprocess
from Bio import SeqIO
from biolite import utils
from gabi.graph import Graph


def read_reference(filename):
    for record in SeqIO.parse(open(filename), 'fasta'):
        yield str(record.seq)
        yield str(record.seq.reverse_complement())


def color_nodes(kmers, graph, reference, color):
    """
    """
    colors = {}
    edges = set()
    for refseq in read_reference(reference):
        i = 0
        while i < len(refseq):
            n = kmers.get(refseq[i:i+graph.k])
            if n and refseq[i+graph.k-1:].startswith(graph.seq(n)):
                colors["node%d" % n] = color
                i += len(graph.seq(n)) + graph.k - 1
                extend = True
                while extend:
                    extend = False
                    for n1 in graph.forward(n):
                        if refseq[i:].startswith(graph.seq(n1)):
                            colors["node%d" % n1] = color
                            e = graph.edge(n, n1)
                            colors["edge%d" % e] = color
                            edges.add(e)
                            i += len(graph.seq(n1))
                            extend = True
                            n = n1
                            break
            else:
                i += 1
    return colors, edges


if __name__ == '__main__':

    # check for dot command
    if utils.which('dot') is None:
        utils.die("could not find 'dot' command from graphviz")

    # arguments
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--graph", required=True,
        help="graphml file containing the GABI assembly graph")
    parser.add_argument(
        "--layout", default="neato",
        help="layout engine to call in graphviz dot")
    parser.add_argument(
        "REFERENCES", nargs='+',
        help="FASTA files containing reference sequences")
    args = parser.parse_args()

    # load graph and kmers
    graph = Graph(args.graph)
    kmers = graph.kmers()
    print "found kmer length", graph.k

    palette = ("red", "blue", "forestgreen")

    # search for kmer seeds in each reference and color the nodes/edges
    # contained in the reference
    for i, reference in enumerate(args.REFERENCES):
        name = os.path.splitext(os.path.basename(reference))[0]
        color = '%s penwidth=4.0' % palette[i % 3]
        colors, edges = color_nodes(kmers, graph, reference, color)
        open('compare%d.edges.txt' % i, 'w').write(','.join(map(str, edges)))
        graph.write_dot('compare%d.dot' % i, colors=colors)
        subprocess.check_call(
            ['dot', '-Tpdf', '-K'+args.layout, 'compare%d.dot' % i],
            stdout=open('compare%d.pdf' % i, 'w'))
        subprocess.check_call(
            ['dot', '-Tsvg', '-K'+args.layout, 'compare%d.dot' % i],
            stdout=open('compare%d.svg' % i, 'w'))

