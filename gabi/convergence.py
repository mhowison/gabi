#!/usr/bin/env python
#
# GABI - Genome Assembly by Bayesian Inference
#
# Copyright 2013-2014, Brown University, Providence, RI. All Rights Reserved.
#
# This file is part of GABI.
#
# GABI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# GABI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GABI.  If not, see <http://www.gnu.org/licenses/>.

import matplotlib
matplotlib.use('Agg')
import numpy as np
import sys
from itertools import combinations
from matplotlib.pyplot import *
from gabi import trace
from gabi.graph import Graph

matplotlib.rcParams.update({'font.size': 8})

palette = ('r', 'b', 'g')
palette2 = ('m', 'y', 'c')


def calc_cumulative_frequencies(matrix):
    n = float(matrix.shape[1])
    norm = np.arange(1, n+1, dtype=np.float)
    return np.cumsum(matrix, axis=1) / norm


def convergence_plot(
        likelihoods, edge_frequencies, node_frequencies, outname):
    """
    Generates:
    (a) Mixing behvaior
    (b) Cumulative frequencies of individual nodes/edges in each chain
    (c) Average st. dev. of the cumulative node/edge frequencies across chains
    (d) Split frequencies at the final sample
    """

    nchains = len(likelihoods)
    nsamples = map(len, likelihoods)
    nedges = edge_frequencies[0].shape[0]
    nnodes = node_frequencies[0].shape[0]

    # Calculate average stdev of the split frequencies
    xmin = min(nsamples)
    xmax = max(nsamples)
    stdev = np.zeros(xmin)
    for j in xrange(nedges):
        for i in xrange(xmin):
            stdev[i] += np.std([freq[j,i] for freq in edge_frequencies])
    for j in xrange(nnodes):
        for i in xrange(xmin):
            stdev[i] += np.std([freq[j,i] for freq in node_frequencies])
    stdev *= 1.0 / (nedges + nnodes)

    figure(figsize=(8,6), dpi=300)

    subplot(221)
    title('a', size=12, weight='heavy')
    xlabel('Accepted Samples')
    ylabel('log(likelihood)')
    xlim([-0.02*xmax, 1.02*xmax])
    grid(axis='x')
    gca().set_rasterization_zorder(1)
    for i, y in enumerate(likelihoods):
        label = 'Chain %d' % (i+1)
        plot(y, color=palette[i%3], alpha=0.5, zorder=0, label=label)
    gca().legend(loc='lower right', fontsize=7)

    subplot(222)
    title('b', size=12, weight='heavy')
    xlabel('Accepted Samples')
    ylabel("Cumulative Node/Edge Frequency")
    xlim([-0.02*xmax, 1.02*xmax])
    grid(axis='x')
    gca().set_rasterization_zorder(1)
    for i, freq in enumerate(edge_frequencies):
        for y in freq:
            plot(y, color=palette[i%3], alpha=0.1, zorder=0)
    for i, freq in enumerate(node_frequencies):
        for y in freq:
            plot(y, color=palette[i%3], alpha=0.1, zorder=0)

    subplot(223)
    title('c', size=12, weight='heavy')
    xlabel('Accepted Samples')
    ylabel('Average St. Dev.')
    xlim([-0.02*xmin, 1.02*xmin])
    grid(axis='x')
    plot(stdev, color='k')

    subplot(224)
    title('d', size=12, weight='heavy')
    xlim([0, 1])
    ylim([0, 1])
    if nchains == 2:
        xlabel('Node/Edge Frequency (Chain 1)')
        ylabel('Node/Edge Frequency (Chain 2)')
        x = [edge_frequencies[0][j,-1] for j in xrange(nedges)]
        x += [node_frequencies[0][j,-1] for j in xrange(nnodes)]
        y = [edge_frequencies[1][j,-1] for j in xrange(nedges)]
        y += [node_frequencies[1][j,-1] for j in xrange(nnodes)]
        scatter(x, y, facecolors='none', edgecolors='k')
    else:
        xlabel('Node/Edge Frequency (Chain X)')
        ylabel('Node/Edge Frequency (Chain Y)')
        for i, pair in enumerate(combinations(xrange(nchains), 2)):
            a, b = pair
            x = [edge_frequencies[a][j,-1] for j in xrange(nedges)]
            x += [node_frequencies[a][j,-1] for j in xrange(nnodes)]
            y = [edge_frequencies[b][j,-1] for j in xrange(nedges)]
            y += [node_frequencies[b][j,-1] for j in xrange(nnodes)]
            scatter(
                x, y, label='Chain %d vs. %d'%(a+1, b+1),
                facecolors='none', edgecolors=palette2[i])
        gca().legend(loc='lower right', fontsize=7)

    tight_layout()
    savefig(outname, dpi=300)


if __name__ == '__main__':
    graph = Graph(sys.argv[1])
    likelihoods = []
    edge_frequencies = []
    node_frequencies = []
    for path in sys.argv[2:]:
        trace.load(path)
        likelihoods.append(list(trace.get_likelihoods()))
        matrices = trace.get_incidence_matrices(graph.E, graph.N)
        edge_frequencies.append(calc_cumulative_frequencies(matrices[0]))
        node_frequencies.append(calc_cumulative_frequencies(matrices[1]))
    convergence_plot(
        likelihoods, edge_frequencies, node_frequencies, 'convergence.pdf')

