#!/usr/bin/env python
#
# GABI - Genome Assembly by Bayesian Inference
#
# Copyright 2013-2014, Brown University, Providence, RI. All Rights Reserved.
#
# This file is part of GABI.
#
# GABI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# GABI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GABI.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import os
import string
from biolite import diagnostics
from biolite import report
from biolite import utils
from gabi import consensus
from gabi import convergence
from gabi import trace
from gabi.graph import Graph

html_template = """<!DOCTYPE html>
<head>
<meta http-equiv="content-type" content="text/html;charset=utf-8">
<title>%s</title>
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<style type="text/css">
body {
  margin: 30px 50px;
}
.stats {
  margin: 10px;
  display: none;
}
.table {
  font-size: 14px;
  font-family: Monaco,Menlo,Consolas,"Courier New",monospace;
}
.table-mini {
  width: auto;
}
table.layout tr {
  border-top: 1px solid #eee;
  border-bottom: 1px solid #eee;
}
blockquote p { font-size: 14px; }
.axis path, .axis line {
  fill: none;
  stroke: black;
  shape-rendering: crispEdges;
}
.axis text {
  font-family: sans-serif;
  font-size: 10px;
}
</style>
</head>
<body>
%s
</body>
</html>
"""

animation_template = """
<h2 id="trace-header">Loading...</h2>
<h5 style="text-align:center">log(likelihood)</h5>
<div id="trace"></div>
<p style="text-align:center" id="step"></p>
<blockquote>Mouse over the plot to see the assembly of a particular sample.</blockquote>
</div>
<div id="assembly" style="width:100%%; float:left"></div>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/d3.min.js"></script>
<script type="text/javascript" src="run%d.data.js"></script>
<script type="text/javascript">
var assembly = d3.select("#assembly");

// Colors
var palette = [
  [186,186,186],
  [253,174,97],
  [215,25,28]
  ];

function colormap(x) {
  x *= (palette.length - 1);
  var lo = Math.floor(x);
  var rgb = [];
  if (lo < palette.length - 1) {
    var mid = x - lo;
    var hi = lo + 1;
    rgb[0] = Math.floor(palette[lo][0] + mid * (palette[hi][0] - palette[lo][0]));
    rgb[1] = Math.floor(palette[lo][1] + mid * (palette[hi][1] - palette[lo][1]));
    rgb[2] = Math.floor(palette[lo][2] + mid * (palette[hi][2] - palette[lo][2]));
  } else {
    rgb[0] = palette[lo][0];
    rgb[1] = palette[lo][1];
    rgb[2] = palette[lo][2];
  }
  return "rgb(" + rgb.join(",") + ")";
}

// Animation state

var i = 0;
var end = likelihoods.length - 1;
var playing = false;

// Trace plot

var height = 200;
var width = $("#summary-table").width() - 30;
var trace = d3.select("#trace").append("svg:svg").attr("width", width+30).attr("height", height+30);
var x;
var y;

function clear_state() {
  assembly.selectAll(".node").selectAll("ellipse").attr("stroke", "grey").attr("stroke-width", "2");
  assembly.selectAll(".edge").selectAll("path").attr("stroke", "grey").attr("stroke-width", "2");
}

function color_state() {
  for (var j=0; j<node_frequencies[i].length; j++) {
    var color = colormap(node_frequencies[i][j]);
    assembly.select("#node"+j).selectAll("ellipse").attr("stroke", color);
  }
  for (var j=0; j<nodes[i].length; j++) {
    assembly.select("#node"+nodes[i][j]).selectAll("ellipse").attr("stroke-width", "6");
  }
  for (var j=0; j<edge_frequencies[i].length; j++) {
    var color = colormap(edge_frequencies[i][j]);
    assembly.select("#edge"+j).selectAll("path").attr("stroke", color);
  }
  for (var j=0; j<edges[i].length; j++) {    assembly.select("#edge"+edges[i][j]).selectAll("path").attr("stroke-width", "4");
  }
}

function redraw() {
  d3.select("#step").text("Sample #:  "+i);
  trace.select("#marker").attr("d", "M"+(x(i)+30)+",0V"+height);
  clear_state();
  color_state();
}

function step() {
  redraw();
  i = (i + 1) %% end;
}

var interval;
function animate() {
  if (playing) {
    playing = false;
    interval = window.clearInterval(interval);
  } else {
    playing = true;
    interval = setInterval(step, 33);
  }
}

function trace_hover() {
  if (!playing) {
    j = Math.round(x.invert(d3.mouse(this)[0]-30));
    if (i != j && j >= 0) {
      i = j;
      redraw();
    }
  }
}

// Trace plot

x = d3.scale.linear().domain([0, likelihoods.length - 1]).range([0,width]);
y = d3.scale.linear().domain([d3.min(likelihoods), d3.max(likelihoods)]).range([height,0]);
trace.selectAll("path.line").data([likelihoods]).enter().append("svg:path").attr("d", d3.svg.line().x(function(d,i) { return  x(i); }).y(function(d) { return y(d); })).attr("stroke", "blue").attr("stroke-width", 0.5).attr("fill", "none").attr("transform", "translate(30,0)");
trace.append("path").attr("id", "marker").attr("d", "M30,0V"+height).attr("stroke", 'red');
var xAxis = d3.svg.axis().scale(x).orient("bottom");
var yAxis = d3.svg.axis().scale(y).orient("left");
trace.append("g").attr("class","axis").attr("transform", "translate(30," + height + ")").call(xAxis);
trace.append("g").attr("class","axis").attr("transform", "translate(30,0)").call
(yAxis);

// Assembly graph

function appendStringAsNodes(id, xml) {
    var frag = document.createDocumentFragment(),
        tmp = document.createElement('body'), child;
    tmp.innerHTML = xml;
    // Append elements in a loop to a DocumentFragment, so that the browser does
    // not re-render the document for each node
    while (child = tmp.firstChild) {
        frag.appendChild(child);
    }
    d3.select(id).node().appendChild(frag); // Now, append all elements at once
    frag = tmp = null;
}
appendStringAsNodes("#assembly", assembly_svg);

assembly_svg = assembly.select("svg");
var aspect = parseInt(assembly_svg.attr("width")) / parseInt(assembly_svg.attr("height"));
width = 0.99 * ($("#assembly").width() - width - 100);
var height = aspect*width;
$("#assembly").width(width).height(height);
assembly_svg.attr("width", width).attr("height", height);

clear_state();
color_state();

// Mouse events

assembly.on("click", animate);
trace.on("click", animate);
trace.on("mousemove", trace_hover);

d3.select("#trace-header").text("Trace Animation");
d3.select("#step").text("Sample #:  0");

</script>
"""


def lookup_args(run_ids):
    """
    Lookup the runs in the diagostics database, and associated paths and
    arguments
    """
    graphml = None
    args = {}
    for run_id in sorted(run_ids):
        run = diagnostics.lookup_run(run_id)
        if run.name != 'sample':
            utils.die("run", run_id, "is not a 'sample' run")
        init = diagnostics.lookup(run_id, diagnostics.INIT)
        if not init or 'graphml' not in init:
            utils.die("could not load diagnostics for run", run_id)
        if graphml is None:
            graphml = init['graphml']
        elif graphml != init['graphml']:
            utils.die("run", run_id, "was based on a different graph")
        path = diagnostics.lookup(run_id, "sample.sample.trace").get("path")
        if not path:
            utils.die("couldn't find trace path for run", run_id)
        init['run'] = run
        init['trace'] = path
        args[run_id] = init
    return args


def animation_data(
        svg, likelihoods, edge_frequencies, node_frequencies, prefix):
    """
    """
    array_str = lambda x: '[' + ','.join(map("{:.2f}".format, x)) + ']'
    with open(prefix+".data.js", 'w') as f:
        f.write("var assembly_svg = '")
        f.writelines(map(string.rstrip, open(svg)))
        f.write("';\n")
        f.write("var likelihoods = [")
        f.write(','.join(map("{:.2f}".format, likelihoods)))
        f.write("];\n")
        f.write("var edges = [")
        f.write(','.join(trace.get_edges()))
        f.write("];\n")
        f.write("var nodes = [")
        f.write(','.join(trace.get_nodes()))
        f.write("];\n")
        f.write("var edge_frequencies = [")
        f.write(','.join(map(array_str, edge_frequencies.transpose())))
        f.write("];\n")
        f.write("var node_frequencies = [")
        f.write(','.join(map(array_str, node_frequencies.transpose())))
        f.write("];\n")


def report_run(run_id, args, graph, prefix):

    table = [
        ('Catalog ID', args['run'].id),
        ('Timestamp', args['run'].timestamp),
        ('Expected # Contigs', args['contigs']),
        ('Expected Genome Size', '{:,} bp'.format(int(args['genome_size']))),
        ('# Samples', '{:,.0f}'.format(args['nsamples'])),
        ('# Accepted', '{:,} ({:.1f}%)'.format(args['accept'], args['accept_per'])),
        ('Total Sample Time', '{:,.1f} sec'.format(args['time'])),
        ('Min. Sample Time', '%.1f sec' % args['mintime']),
        ('Max. Sample Time', '%.1f sec' % args['maxtime']),
        ('Mean Sample Time', '%.1f sec' % (args['time'] / args['nsamples']))]
    row = '<tr><td><b>{}</b></td><td>{}</td></tr>'
    table = '\n'.join(row.format(k, v) for k, v in table)

    title = 'GABI Run %d' % run_id
    body = [
        '<div id="wrapper" style="float:left">',
        '<h1><em>%s</em></h1>' % title,
        '<blockquote>',
        '<table id="summary-table" class="table table-striped table-condensed table-mini">',
        table,
        '</table></blockquote>',
        animation_template % run_id]

    # output plots and html
    open(prefix+".html", 'w').write(html_template % (title, '\n'.join(body)))

    return table


def report_all(run_ids, outdir):
    args = lookup_args(run_ids)
    graph = Graph(args[run_ids[0]]['graphml'])
    likelihoods = []
    edge_frequencies = []
    node_frequencies = []

    title = "GABI Report (Runs %s)" % ','.join(utils.number_range(run_ids))
    body = [
        '<h1><em>%s</em></h1>' % title,
        '<blockquote>',
        'Summary report for multiple GABI runs. Click on a Run header',
        'below for a detailed interactive trace for that run.',
        '</blockquote>',
        '<table class="layout">']

    # Stats and traces for each run.
    for i in run_ids:
        prefix = os.path.join(outdir, "run%d" % i)
        trace.load(args[i]['trace'])
        trace.get_stats(args[i])
        likelihoods.append(list(trace.get_likelihoods()))
        matrices = trace.get_incidence_matrices(graph.E, graph.N)
        frequencies = consensus.calc_frequencies(*matrices)
        args[i]['frequencies'] = frequencies
        consensus.posterior_plot(graph, frequencies, prefix)
        consensus.majority_rule_plot(graph, frequencies, 0.5, prefix)
        edge_frequencies.append(
            convergence.calc_cumulative_frequencies(matrices[0]))
        node_frequencies.append(
            convergence.calc_cumulative_frequencies(matrices[1]))
        animation_data(
            prefix+"-posteriors.svg", likelihoods[-1],
            edge_frequencies[-1], node_frequencies[-1], prefix)
        table = report_run(i, args[i], graph, prefix)
        body += [
            '<tr><td valign="top">',
            '<h3><a href="run{0}.html">Run {0}</a></h3>'.format(i),
            '<blockquote>',
            '<table class="table table-striped table-condensed table-mini">',
            table,
            '</td></table></blockquote>',
            '<td align="center" valign="top" style="padding-left:20px;">',
            '<a href="run{0}.html"><img src="run{0}-posteriors.svg" width="400" height="400"/></a>'.format(i),
            '<h5>Posterior Distribution</h5></td>',
            '<td align="center" valign="top" style="padding-left:20px;">',
            '<a href="run{0}.html"><img src="run{0}-majority.svg" width="400" height="400"/></a>'.format(i),
            '<h5>Majority-Rule Consensus [<a href="run%d-majority.fa">FASTA</a>]</h5></td>' % i,
            '</tr>']

    if len(run_ids) > 1:
        # Combine values from multiple runs.
        combined = {
            'nsamples': sum(args[i]['nsamples'] for i in run_ids),
            'time': sum(args[i]['time'] for i in run_ids),
            'mintime': min(args[i]['mintime'] for i in run_ids),
            'maxtime': max(args[i]['maxtime'] for i in run_ids),
            'accept': sum(args[i]['accept'] for i in run_ids)}
        combined['accept_per'] = \
            100.0 * combined['accept'] / combined['nsamples']
        frequencies = {}
        for i in run_ids:
            for k, v in args[i]['frequencies'].iteritems():
                frequencies[k] = frequencies.get(k, 0) + v * args[i]['accept']
        for k in frequencies:
            frequencies[k] = frequencies[k] / combined['accept']
        table = [
            ('# Samples', '{:,.0f}'.format(combined['nsamples'])),
            ('# Accepted', '{:,} ({:.1f}%)'.format(
                combined['accept'], combined['accept_per'])),
            ('Total Sample Time', '{:,.1f} sec'.format(combined['time'])),
            ('Min. Sample Time', '%.1f sec' % combined['mintime']),
            ('Max. Sample Time', '%.1f sec' % combined['maxtime']),
            ('Mean Sample Time', '%.1f sec' % (combined['time'] / combined['nsamples']))]
        row = '<tr><td><b>{}</b></td><td>{}</td></tr>'
        table = '\n'.join(row.format(k, v) for k, v in table)
        prefix = os.path.join(outdir, 'run-all')
        consensus.posterior_plot(graph, frequencies, prefix)
        consensus.majority_rule_plot(graph, frequencies, 0.5, prefix)
        body += [
            '<tr><td valign="top">',
            '<h3>All Runs</h3>',
            '<blockquote>',
            '<table class="table table-striped table-condensed table-mini">',
            table,
            '</td></table></blockquote>',
            '<td align="center" valign="top" style="padding-left:20px;">',
            '<img src="run-all-posteriors.svg" width="400" height="400"/>',
            '<h5>Posterior Distribution</h5></td>',
            '<td align="center" valign="top" style="padding-left:20px;">',
            '<img src="run-all-majority.svg" width="400" height="400"/>',
            '<h5>Majority-Rule Consensus [<a href="run-all-majority.fa">FASTA</a>]</h5></td>',
            '</tr>',
            '</table>']

        convergence.convergence_plot(
            likelihoods, edge_frequencies, node_frequencies,
            os.path.join(outdir, 'convergence.svg'))
        body += [
            '<h3>Convergence</h3>',
            '<img src="convergence.svg" width="800" height="600"/>',
            '<p><b>(a)</b> Mixing behvaior',
            '<br/><b>(b)</b> Cumulative frequencies of individual edges in each chain',
            '<br/><b>(c)</b> Average st. dev. of the cumulative edge frequencies across chains',
            '<br/><b>(d)</b> Split frequencies at the final sample</p>']

        body += [
            '<h3>Node Frequencies</h3>',
            '<table class="table table-striped table-condensed table-mini">',
            '<tr><th>Node ID</th><th>Frequency</th><th>Sequence Length</th></tr>']
        for key in sorted(frequencies, key=frequencies.get, reverse=True):
            if key.startswith("node"):
                n = int(key[4:])
                body.append('<tr><td>%d</td><td>%.1f%%</td><td>%d</td></tr>' % (
                    n, 100.0*frequencies[key], len(graph.seq(n))))
        body.append('</table>')

    else:
        body.append('</table>')

    # Final output.
    report.copy_css(outdir)
    report.copy_js(outdir)
    html = html_template % (title, '\n'.join(body))
    open(os.path.join(outdir, 'index.html'), 'w').write(html)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="""
        Generates an HTML report of all runs in the agalma diagnostics database
        for the given agalma catalog ID (by default), or of only the specific
        list of RUN_IDS""")
    parser.add_argument('--outdir', '-o', default='./', type=utils.safe_mkdir,
        help="write HTML output to OUTDIR [default: ./]")
    parser.add_argument('run_ids', type=int,  metavar='RUN_ID', nargs='+',
        help="a list of 'sample' pipeline run IDs")
    args = parser.parse_args()
    # Check for graphviz
    if utils.which('dot') is None:
        utils.die("could not find 'dot' command from graphviz")
    report_all(sorted(args.run_ids), args.outdir)

